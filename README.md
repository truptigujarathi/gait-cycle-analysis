# GAIT Cycle Analysis

This repo contains the code executed in MATLAB for analysis of GAIT Cycle using IMU Sensor
Angular rate data is obtained from sensors attached on body segments.
Obtained Data is stored in Excel sheet file format, which is further imported in MATLAB.
Gait parameters for example stride time, stance time, swing time, cadence, gait speed etc. are calculated using MATLAB code.
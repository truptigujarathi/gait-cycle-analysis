% Author : Trupti Gujarathi
% Date: 16/02/2019
%  Matlab based code for GAIT cycle analysis project 
%
clc
clear

%Read sensor data stored in Excel
x = xlsread('gaitcycle.xlsx','sanket','(E2:E2418)');     %time in miliseconds as x-axis
RightF = xlsread('gaitcycle.xlsx','sanket','(F2:F2418)');   % angular rate obtained from Right shank sensor
LeftF = xlsread('gaitcycle.xlsx','sanket','(G2:G2418)');    % angular rate obtained from Left shank sensor
    
%    These variables are chosen for use in peakdet. They define what is considered a peak.    
    delta_RightF=30;
    delta_LeftF=10;
    
%    This finds the Maximum value(Toe-Off) and minimum value(Heel strike) for the real and 
%    imaginary admittances using an opensource function called peakdet.
    [Max_RF, Min_RF]=peakdet1(RightF, delta_RightF);
    [Max_LF, Min_LF]=peakdet1(LeftF, delta_LeftF);
     
    lgth_RightF =(length(Max_RF))-1;
    distance_btwn_pos_peaks_RightF = zeros(lgth_RightF,1);
     
    lgth_LeftF =(length(Max_LF))-1;
    distance_btwn_pos_peaks_LeftF = zeros(lgth_LeftF,1);

%Make a loop that describes the distance in x between the peaks. 
    
%     for i=1:lgth_RightF
%         
%         x1_t1(i)  = x(Max_RF(i,1));
%         x2_t1(i) = x(Max_RF(i+1,1));
%         distance_btwn_pos_peaks_RightF(i,1) = x2_t1(i)-x1_t1(i);
%     end
%     %Make a loop that describes the distance in x between the peaks.
%     for j=1:lgth_LeftF
%         
%         x1_t2(j)= x(Max_LF(j,1));
%         x2_t2(j) =x(Max_LF(j+1,1));
%         
%         distance_btwn_pos_peaks_LeftF(j,1) = x2_t2(j)-x1_t2(j);
%     end


% Gait Temporal Parameters 
  for i=1:lgth_RightF
  Rstride(i,1) = -x(Min_RF(i))+x(Min_RF(i+1));      % time required to complete single stride 
  Rswing(i,1) =  -x(Max_RF(i)) +x(Min_RF(i));       % time required to complete swing 
  Rstance(i,1)=  (Rstride(i)- Rswing(i));           % time required to complete stance phase 
  end
  for i=1:lgth_LeftF
   Rtstep(i,1) = -(x(Min_RF(i)) -  x(Min_LF(i)));
  end
  
  
% Gait Spatial Parameters
% Distance taken for assessment is 40m.
Total_Distance = 40;     %in Meter
p=numel(Rstride);        % Total no.of stride occured for the given distance
StrideLength = Total_Distance/p;  %in Meter
StepLength =    StrideLength/2 ;  % in Meter


AvgStride_time = mean(Rstride);     % time required to complete single gait cycle i.e stride

AvgSteptime = mean(Rtstep);         % time required to single step

Cadence = 60/AvgSteptime;           % No. of gait cycles completed per minute

Stride_Speed = StrideLength/AvgStride_time;

Gait_speed = Total distance/Total time 
